//1

const paragraphs = document.getElementsByTagName('p');
[...paragraphs].forEach(item => item.style.backgroundColor="#ff0000")

//2

const optionId = document.getElementById('optionsList');
console.log(optionId);
const optionIdParent = optionId.parentNode;
console.log(optionIdParent);

if(optionId.previousElementSibling){    
    console.log('previousElementSibling:', optionId.previousElementSibling);
}
if(optionId.nextElementSibling){    
    console.log('nextElementSibling:', optionId.nextElementSibling);
}
if(optionId.firstElementChild){    
    console.log('firstElementChild:', optionId.firstElementChild);
}
if(optionId.lastElementChild){    
    console.log('lastElementChild:', optionId.lastElementChild);
}

//3

const testParagraph = document.getElementById('testParagraph');
console.log(testParagraph)
testParagraph.innerHTML = '<p>This is a paragraph<p/>'

//4

const liList = document.querySelectorAll('.main-header li');
console.log(liList);
liList.forEach(item => item.classList.add('nav-item'));

//5

const sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach(item => item.classList.remove('section-title'));



